package com.replaymod.recording.gui;

import static com.replaymod.core.ReplayMod.TEXTURE;
import static com.replaymod.core.ReplayMod.TEXTURE_SIZE;

import cc.hyperium.event.RenderHUDEvent;
import com.google.common.eventbus.Subscribe;
import com.replaymod.core.SettingsRegistry;
import com.replaymod.core.utils.ReplayEventBus;
import com.replaymod.recording.Setting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;

/**
 * Renders overlay during recording.
 */
public class GuiRecordingOverlay {
    private final Minecraft mc;
    private final SettingsRegistry settingsRegistry;

    public GuiRecordingOverlay(Minecraft mc, SettingsRegistry settingsRegistry) {
        this.mc = mc;
        this.settingsRegistry = settingsRegistry;
    }

    public void register() {
        ReplayEventBus.INSTANCE.register(this);
    }

    public void unregister() {
        ReplayEventBus.INSTANCE.unregister(this);
    }

    /**
     * Render the recording icon and text in the top left corner of the screen.
     * @param event Rendered post game overlay
     */
    @Subscribe
    public void renderRecordingIndicator(RenderHUDEvent event) {
        if (settingsRegistry.get(Setting.INDICATOR)) {
            FontRenderer fontRenderer = mc.fontRendererObj;
            fontRenderer.drawString(I18n.format("replaymod.gui.recording").toUpperCase(), 30, 18 - (fontRenderer.FONT_HEIGHT / 2), 0xffffffff);
            mc.getTextureManager().bindTexture(TEXTURE);
            //((TextureManager) ReflectionHelper.getField(mc, "renderEngine", "R").get()).bindTexture(TEXTURE);
            GlStateManager.resetColor();
            GlStateManager.enableAlpha();
            Gui.drawModalRectWithCustomSizedTexture(10, 10, 58, 20, 16, 16, TEXTURE_SIZE, TEXTURE_SIZE);
        }
    }
}
