package com.replaymod.compat.oranges17animations;

import cc.hyperium.event.EntityRenderEvent;
import com.google.common.eventbus.Subscribe;
import com.replaymod.replay.camera.CameraEntity;
import net.minecraft.client.Minecraft;

/**
 * Orange seems to have copied vast parts of the RendererLivingEntity into their ArmorAnimation class which cancels the RenderLivingEvent.Pre and calls its own code instead.
 * This breaks our mixin which assures that, even though the camera is in spectator mode, it cannot see invisible entities.
 * <p>
 * To fix this issue, we simply cancel the RenderLivingEvent.Pre before it gets to ArmorAnimation if the entity is invisible.
 */
public class HideInvisibleEntities {

    @Subscribe
    public void preRenderLiving(EntityRenderEvent event) {
        if (Minecraft.getMinecraft().thePlayer instanceof CameraEntity) {
        }
    }
}
