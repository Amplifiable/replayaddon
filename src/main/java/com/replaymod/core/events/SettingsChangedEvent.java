package com.replaymod.core.events;

import com.replaymod.core.SettingsRegistry;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SettingsChangedEvent {
    @Getter
    private final SettingsRegistry settingsRegistry;

    @Getter
    private final SettingsRegistry.SettingKey<?> key;
}
