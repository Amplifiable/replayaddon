package com.replaymod.editor;

import cc.hyperium.event.PreInitializationEvent;
import com.google.common.eventbus.Subscribe;
import com.replaymod.core.ReplayMod;
import com.replaymod.online.Setting;
import org.apache.logging.log4j.Logger;

public class ReplayModEditor {
    public static final String MOD_ID = "replaymod-editor";

    public static ReplayModEditor instance;

    private ReplayMod core;

    public static Logger LOGGER;

    @Subscribe
    public void preInit(PreInitializationEvent event) {
        core = ReplayMod.instance;

        core.getSettingsRegistry().register(Setting.class);
    }

    public ReplayMod getCore() {
        return core;
    }
}
