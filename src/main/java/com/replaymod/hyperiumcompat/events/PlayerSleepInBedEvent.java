package com.replaymod.hyperiumcompat.events;

import lombok.RequiredArgsConstructor;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;

@RequiredArgsConstructor
public class PlayerSleepInBedEvent {
    public final EntityPlayer entityPlayer;
    public final BlockPos pos;
}
