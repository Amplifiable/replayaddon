package com.replaymod.hyperiumcompat.events;

import lombok.RequiredArgsConstructor;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;

@RequiredArgsConstructor
public class MinecartInteractEvent {
    public final EntityPlayer player;
    public final EntityMinecart minecart;
}
