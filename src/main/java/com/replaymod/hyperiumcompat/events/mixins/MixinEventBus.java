package com.replaymod.hyperiumcompat.events.mixins;

import cc.hyperium.event.EventBus;
import com.replaymod.core.utils.ReplayEventBus;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = EventBus.class, remap = false)
public class MixinEventBus {
  @Inject(method = "post", at = @At("RETURN"), remap = false)
  public void onPost(Object event, CallbackInfo ci) {
    ReplayEventBus.R_INSTANCE.onEvent(event);
  }
}
