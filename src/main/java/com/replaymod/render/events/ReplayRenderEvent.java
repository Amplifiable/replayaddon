package com.replaymod.render.events;

import cc.hyperium.event.CancellableEvent;
import com.replaymod.render.rendering.VideoRenderer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class ReplayRenderEvent extends CancellableEvent {
    @Getter
    private final VideoRenderer videoRenderer;

    public static class Pre extends ReplayRenderEvent {
        public Pre(VideoRenderer videoRenderer) {
            super(videoRenderer);
        }
    }

    public static class Post extends ReplayRenderEvent {
        public Post(VideoRenderer videoRenderer) {
            super(videoRenderer);
        }
    }
}
